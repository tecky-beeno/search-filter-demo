import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable('cat', table => {
    table.setNullable('parent_cat_id')
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('cat', table => {
    table.dropNullable('parent_cat_id')
  })
}
