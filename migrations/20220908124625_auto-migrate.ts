import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('cat'))) {
    await knex.schema.createTable('cat', table => {
      table.increments('id')
      table.text('name').notNullable()
      table.integer('parent_cat_id').unsigned().notNullable().references('cat.id')
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('cat')
}
