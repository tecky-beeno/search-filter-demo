import express from 'express'

let app = express()

app.get('/offer/search', (req, res) => {
  let cat_id = req.query.cat_id

  let cat_ids = Array.isArray(cat_id) ? cat_id : cat_id ? [cat_id] : []

  let whereStr = cat_ids
    .map(s => +s)
    .filter(id => id)
    .map(id => `cat_id = ${id}`)
    .join(' or ')

  let sql = /* sql */ `
select
  mua.id
, "user".nickname
, "user".avatar
, mua.bio
from mua
inner join "user" on "user".id = mua.id
where mua.id in (
  select
    distinct mua_id
  from offer
  ${whereStr ? 'where ' + whereStr : ''}
)
order by mua.score desc
limit 20
`
  console.log(sql)
})
