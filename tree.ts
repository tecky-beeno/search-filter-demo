import { db } from './db'
import { Cat } from './proxy'

let select_cat = db.prepare('select * from cat')

type CatNode = {
  id: number
  name: string
  children: CatNode[]
}

let cats: Map<number, CatNode> = new Map()

let rootCats: CatNode[] = []

select_cat.all().forEach((catRow: Cat) => {
  console.log('catRow:', catRow)
  let id = catRow.id!
  let catNode: CatNode = {
    id,
    name: catRow.name,
    children: [],
  }
  cats.set(id, catNode)
  if (catRow.parent_cat_id) {
    // not root
    let parent = cats.get(catRow.parent_cat_id)
    if (parent) {
      parent.children.push(catNode)
    }
  } else {
    // is root
    rootCats.push(catNode)
  }
})

console.log('rootCats:')
console.dir(rootCats, { depth: 20 })
