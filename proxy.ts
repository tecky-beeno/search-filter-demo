import { proxySchema } from 'better-sqlite3-proxy'
import { db } from './db'

export type Cat = {
  id?: number | null
  name: string
  parent_cat_id: number | null
  parent_cat?: Cat
}

export type DBProxy = {
  cat: Cat[]
}

export let proxy = proxySchema<DBProxy>({
  db,
  tableFields: {
    cat: [
      /* foreign references */
      ['parent_cat', { field: 'parent_cat_id', table: 'cat' }],
    ],
  },
})
